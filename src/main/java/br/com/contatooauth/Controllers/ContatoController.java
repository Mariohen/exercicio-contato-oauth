package br.com.contatooauth.Controllers;

import br.com.contatooauth.Models.Contato;
import br.com.contatooauth.Security.Usuario;
import br.com.contatooauth.Services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping("/contato")
    @ResponseStatus(HttpStatus.CREATED)
    public Contato criar(@RequestBody Contato contato, @AuthenticationPrincipal Usuario usuario){
//        Contato contato2 = new Contato();
        contato.setNomeContato(usuario.getName());
        System.out.println(contato.getTelefone());
        Contato objetoContato = contatoService.criar(contato);
      return contato;
    }

    @GetMapping("/contatos")
    public Iterable<Contato> lerTodosOsContatos(@AuthenticationPrincipal Usuario usuario){
//        Contato contato = new Contato();
//        contato.setNomeContato(usuario.getName());
        return contatoService.pesquisarPeloNomeContato(usuario.getName());
    }
}
