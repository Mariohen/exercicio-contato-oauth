package br.com.contatooauth.Repositories;

import br.com.contatooauth.Models.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {
   List<Contato> findByNomeContato(String nomeContato);
}
