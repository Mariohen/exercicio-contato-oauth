package br.com.contatooauth.Services;

import br.com.contatooauth.Models.Contato;
import br.com.contatooauth.Repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato criar(Contato contato) {
        System.out.println(contato.getTelefone());
        Contato objetoContato = contatoRepository.save(contato);
        return contato;
    }

    public List<Contato> pesquisarPeloNomeContato(String nomeContato){
        return contatoRepository.findByNomeContato(nomeContato);
    }

}
